package xyz.stepsecret.ioiorservice;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.*;
import android.util.Log;
import android.view.Window;

import com.androidplot.Plot;
import com.androidplot.util.PlotStatistics;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.*;

import org.jtransforms.fft.DoubleFFT_1D;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    private static final int HISTORY_SIZE_CH = 100;
    private static final int HISTORY_SIZE_FFT = 100;

    private XYPlot aprHistoryPlot = null;
    private XYPlot aprHistoryPlot2 = null;

    private SimpleXYSeries CH1_Series = null;
    private SimpleXYSeries CH2_Series = null;
    private SimpleXYSeries CH3_Series = null;
    private SimpleXYSeries CH4_Series = null;
    private SimpleXYSeries CH5_Series = null;
    private SimpleXYSeries CH6_Series = null;

    private SimpleXYSeries FFT_Series_Real_1 = null;
    private SimpleXYSeries FFT_Series_Real_2 = null;
    private SimpleXYSeries FFT_Series_Real_3 = null;
    private SimpleXYSeries FFT_Series_Real_4 = null;
    private SimpleXYSeries FFT_Series_Real_5 = null;
    private SimpleXYSeries FFT_Series_Real_6 = null;

    private Redrawer redrawer;
    private Redrawer FFT_redrawer;

    private BoundService mBoundService;
    private boolean mServiceBound = false;

    private IntentFilter mIntentFilter;

    private List<Double> buf_1 = new ArrayList<Double>();
    private List<Double> buf_2 = new ArrayList<Double>();
    private List<Double> buf_3 = new ArrayList<Double>();
    private List<Double> buf_4 = new ArrayList<Double>();
    private List<Double> buf_5 = new ArrayList<Double>();
    private List<Double> buf_6 = new ArrayList<Double>();

    private Boolean Check_plot = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("velocity");
        mIntentFilter.addAction("ioio_connect");
        mIntentFilter.addAction("ioio_disconnect");
        mIntentFilter.addAction("ioio_loop");

        Inital_1();
        Inital_2();

    }


    public void Inital_1()
    {
        // setup the APR History plot:
        aprHistoryPlot = (XYPlot) findViewById(R.id.aprHistoryPlot);

        CH1_Series = new SimpleXYSeries("CH1");
        CH1_Series.useImplicitXVals();
        CH2_Series = new SimpleXYSeries("CH2");
        CH2_Series.useImplicitXVals();
        CH3_Series = new SimpleXYSeries("CH3");
        CH3_Series.useImplicitXVals();
        CH4_Series = new SimpleXYSeries("CH4");
        CH4_Series.useImplicitXVals();
        CH5_Series = new SimpleXYSeries("CH5");
        CH5_Series.useImplicitXVals();
        CH6_Series = new SimpleXYSeries("CH6");
        CH6_Series.useImplicitXVals();

        aprHistoryPlot.setRangeBoundaries(-2, 7, BoundaryMode.FIXED);
        aprHistoryPlot.setDomainBoundaries(0, HISTORY_SIZE_CH, BoundaryMode.FIXED);
        aprHistoryPlot.addSeries(CH1_Series,new LineAndPointFormatter(Color.rgb(255, 0, 0), null, null, null));
        aprHistoryPlot.addSeries(CH2_Series,new LineAndPointFormatter(Color.rgb(255, 204, 0), null, null, null));
        aprHistoryPlot.addSeries(CH3_Series,new LineAndPointFormatter(Color.rgb(255, 102, 102), null, null, null));
        aprHistoryPlot.addSeries(CH4_Series,new LineAndPointFormatter(Color.rgb(255, 204, 153), null, null, null));
        aprHistoryPlot.addSeries(CH5_Series,new LineAndPointFormatter(Color.rgb(102, 255, 51), null, null, null));
        aprHistoryPlot.addSeries(CH6_Series,new LineAndPointFormatter(Color.rgb(0, 255, 255), null, null, null));

        aprHistoryPlot.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        aprHistoryPlot.setDomainStepValue(HISTORY_SIZE_CH/10);
        aprHistoryPlot.setLinesPerRangeLabel(3);
        aprHistoryPlot.setDomainLabel("Index");
        aprHistoryPlot.getDomainTitle().pack();
        aprHistoryPlot.setRangeLabel("Volt.");
        aprHistoryPlot.getRangeTitle().pack();

        aprHistoryPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        aprHistoryPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));

        // setup checkboxes:

        final PlotStatistics histStats = new PlotStatistics(1000, false);

        aprHistoryPlot.addListener(histStats);

        // get a ref to the BarRenderer so we can make some changes to it:

        redrawer = new Redrawer(
                Arrays.asList(new Plot[]{aprHistoryPlot}),
                100, false);
    }

    public void Inital_2()
    {
        // setup the APR History plot:
        aprHistoryPlot2 = (XYPlot) findViewById(R.id.aprHistoryPlot2);


        FFT_Series_Real_1 = new SimpleXYSeries("1");
        FFT_Series_Real_1.useImplicitXVals();
        FFT_Series_Real_2 = new SimpleXYSeries("2");
        FFT_Series_Real_2.useImplicitXVals();
        FFT_Series_Real_3 = new SimpleXYSeries("3");
        FFT_Series_Real_3.useImplicitXVals();
        FFT_Series_Real_4 = new SimpleXYSeries("4");
        FFT_Series_Real_4.useImplicitXVals();
        FFT_Series_Real_5 = new SimpleXYSeries("5");
        FFT_Series_Real_5.useImplicitXVals();
        FFT_Series_Real_6 = new SimpleXYSeries("6");
        FFT_Series_Real_6.useImplicitXVals();

        aprHistoryPlot2.setRangeBoundaries(-20, 20, BoundaryMode.FIXED);
        aprHistoryPlot2.setDomainBoundaries(0, HISTORY_SIZE_FFT, BoundaryMode.FIXED);
        aprHistoryPlot2.addSeries(FFT_Series_Real_1,new LineAndPointFormatter(Color.rgb(255, 0, 0), null, null, null));
        aprHistoryPlot2.addSeries(FFT_Series_Real_2,new LineAndPointFormatter(Color.rgb(255, 204, 0), null, null, null));
        aprHistoryPlot2.addSeries(FFT_Series_Real_3,new LineAndPointFormatter(Color.rgb(255, 102, 102), null, null, null));
        aprHistoryPlot2.addSeries(FFT_Series_Real_4,new LineAndPointFormatter(Color.rgb(255, 204, 153), null, null, null));
        aprHistoryPlot2.addSeries(FFT_Series_Real_5,new LineAndPointFormatter(Color.rgb(102, 255, 51), null, null, null));
        aprHistoryPlot2.addSeries(FFT_Series_Real_6,new LineAndPointFormatter(Color.rgb(0, 255, 255), null, null, null));

        aprHistoryPlot2.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        aprHistoryPlot2.setDomainStepValue(HISTORY_SIZE_FFT/5);
        aprHistoryPlot2.setLinesPerRangeLabel(3);
        aprHistoryPlot2.setDomainLabel("Index");
        aprHistoryPlot2.getDomainTitle().pack();
        aprHistoryPlot2.setRangeLabel("Volt.");
        aprHistoryPlot2.getRangeTitle().pack();

        aprHistoryPlot2.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        aprHistoryPlot2.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));

        // setup checkboxes:

        final PlotStatistics histStats = new PlotStatistics(1000, false);

        aprHistoryPlot2.addListener(histStats);

        // get a ref to the BarRenderer so we can make some changes to it:

        FFT_redrawer = new Redrawer(
                Arrays.asList(new Plot[]{aprHistoryPlot2}),
                100, false);
    }


    @Override
    public void onStart() {
        super.onStart();

        Intent intent = new Intent(this, BoundService.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);


    }

    @Override
    public void onResume() {
        super.onResume();
        redrawer.start();
        FFT_redrawer.start();

        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mServiceBound) {
            unbindService(mServiceConnection);
            mServiceBound = false;
        }

    }

    @Override
    public void onPause() {
        redrawer.pause();
        FFT_redrawer.pause();
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onDestroy() {
        redrawer.finish();
        FFT_redrawer.finish();
        super.onDestroy();
    }


    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {

            mServiceBound = false;

        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundService.MyBinder myBinder = (BoundService.MyBinder) service;
            mBoundService = myBinder.getService();
            mServiceBound = true;

        }
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("ioio_connect"))
            {
                Log.e("Broadcast Receiver ","intent.getAction() : ioio_connect");
            }
            else if(intent.getAction().equals("ioio_disconnect"))
            {
                Log.e("Broadcast Receiver ","intent.getAction() : ioio_connect");
            }
            else if(intent.getAction().equals("ioio_loop"))
            {
               // Log.e("Broadcast Receiver ","intent.getAction() : ioio_loop");

                // get rid the oldest sample in history:
                if (CH1_Series.size() > HISTORY_SIZE_CH) {
                    CH1_Series.removeFirst();
                    CH2_Series.removeFirst();
                    CH3_Series.removeFirst();
                    CH4_Series.removeFirst();
                    CH5_Series.removeFirst();
                    CH6_Series.removeFirst();

                    if(!Check_plot)
                    {
                        //Check_plot = true;

                        double[] input_1 = new double[buf_1.size()];

                        for(int i = 0 ; i < buf_1.size() ; i++)
                        {
                            input_1[i] = buf_1.get(i);

                        }

                        buf_1.remove(0);


                        double[] fft = DoFFT(input_1);

                        int length_ = fft.length;
                        for(int i = 0; i < length_ ; i += 2){
                            //double real = fft[2*i];
                            //double imag = fft[2*i+1];

                            if (FFT_Series_Real_1.size() > HISTORY_SIZE_FFT) {
                                FFT_Series_Real_1.removeFirst();
                                //FFT_Series_Real_2.removeFirst();
                                //FFT_Series_Real_3.removeFirst();
                                //FFT_Series_Real_4.removeFirst();
                                //FFT_Series_Real_5.removeFirst();
                                //FFT_Series_Real_6.removeFirst();
                            }

                            Double x = 2.0;

                            FFT_Series_Real_1.addLast(null, Math.sqrt(fft[i] * fft[i] + fft[i + 1] * fft[i + 1])+x*0);
                            //FFT_Series_Real_2.addLast(null, d+x*1);
                            //FFT_Series_Real_3.addLast(null, d+x*2);
                            //FFT_Series_Real_4.addLast(null, d+x*3);
                            //FFT_Series_Real_5.addLast(null, d+x*4);
                            //FFT_Series_Real_6.addLast(null, d+x*5);
                            //Log.e("xxx"," d : "+d);


                        }


                    }



                }


                Double x = 1.0;

                CH1_Series.addLast(null, intent.getFloatExtra("CH1_Volts",0)+x*0);
                CH2_Series.addLast(null, intent.getFloatExtra("CH2_Volts",0)+x*1);
                CH3_Series.addLast(null, intent.getFloatExtra("CH3_Volts",0)+x*2);
                CH4_Series.addLast(null, intent.getFloatExtra("CH4_Volts",0)+x*3);
                CH5_Series.addLast(null, intent.getFloatExtra("CH5_Volts",0)+x*4);
                CH6_Series.addLast(null, intent.getFloatExtra("CH6_Volts",0)+x*5);

                buf_1.add(intent.getFloatExtra("CH1_Volts",0)+0.0);

                //Log.e("xxx"," d1 : "+intent.getFloatExtra("CH1_Volts",0));
                //Log.e("xxx"," d2 : "+intent.getFloatExtra("CH2_Volts",0));
                //Log.e("xxx"," d3 : "+intent.getFloatExtra("CH3_Volts",0));
                //Log.e("xxx"," d4 : "+intent.getFloatExtra("CH4_Volts",0));
                //Log.e("xxx"," d5 : "+intent.getFloatExtra("CH5_Volts",0));
                //Log.e("xxx"," d6 : "+intent.getFloatExtra("CH6_Volts",0));

                //Log.e("xxx"," buf_1 : "+intent.getFloatExtra("CH6_Volts",0)+x*5);
            }

            // Log.e("BroadcastReceiver ","intent.getAction() : "+intent.getAction());
        }
    };

    double[] DoFFT(double[] input)
    {

        int length_ = input.length;

        DoubleFFT_1D fftDo = new DoubleFFT_1D(length_);
        double[] fft = new double[length_ * 2];
        for(int i = 0 ; i  < length_ ; i++)
        {
            fft[2 * i] = input[i];
            fft[2 * i + 1] = 0;
        }
        fftDo.realForwardFull(fft);

        return fft;

    }

}